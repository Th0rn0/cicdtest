# Risk Narrative Tech Test

Initially I looked into DroneCI and AWS CodeDeploy. However due to limitations with the setup (I wanted it to be as easy as possible) and limitations with free tier products I decided to used GitLab CI. I've never used GitLab CI however the syntax seemed fairly straight forward.

I decided to use Terraform to setup the inital infrastructure on AWS.

The Deploy function connects to the AWS EC2 instance and runs update commands on the docker compose file.

The tools and infrastructure was designed to be deployed and work as quickly and as easy as possible with minimal interaction from the end user. Because of this I felt things like GitLab Runners would be out of scope however I would use them given a more permnament solution.

## Further Considerations
If I had more time/budget I would have deployed out ECR repos and used ECS, loadbalancers and target groups to deploy the services with private and public subnets. 

Manual approvals on deployment to staging/production as well as code base linting, tag based releases (only build and deploy to production on a tag release (tagged with the version number) and on push to staging).

I would probably steer clear from a SSH deployment and used the gitlab runners or similar to allow for more Error detection/rollback on the deployment.

RDS would absolutely be used over a docker container. Again this was chosen for ease of deployment and cost.

## Tools used

Terraform - AWS Infrastructure
AWS - VPC and EC2 Deployed into eu-west-1
Gitlab - Repo and CICD
Laravel - Example Application
Docker - PHP, Nginx and MySQL Images

## Setup

- Configure AWS CLI for the correct Environment for Terraform
- cd into the ```terraform``` directory
- run ```terraform init && terraform apply``` to setup the initial AWS Environment to deploy onto
- Add the following variables to the Gitlab CICD substituting xxx with the correct credentials
- - ```AWS_ACCESS_KEY_ID=xxx```
- - ```AWS_SECRET_KEY_ID=xxx```
- - ```AWS_DEFAULT_REGION=eu-west-1```
- - ```INSTANCE_IP=xxx``` - Get this from the output of ```terraform apply```
- - ```APP_KEY``` - Base64 encoded string for laravel. EG "base64:CWSn9huySwDt+uslG/LqW4xkUqzSs6Qjd1MG+ukMO/A="
- - ```SSH_PRIVATE_KEY=xxx``` - Get this from the output of ```terraform output -raw private_key```
![Screenshot](./pictures/gitlabcivariables.png)
- Use SSM Connect or SSH to connect to the EC2 instance and paste in the commands below (Env Variable Section)
- Push to the main branch of the repo
- Visit the EC2 Address that terraform outputs (on port 80)
- You should see a Laravel Welcome Page

## Workflow

- Push to Repo
- CI Kicks Off
- Build Assets (NPM/Composer)
- Build Docker Images
- Test Application with Compiled Docker Images
- Push to Docker Repo
- Connect to Staging Server
- Inject Env Variables from Gitlab CI
- Upload new Docker Compose File
- Pull Docker Images
- Restart Application



## Env Variables for Server

Run on EC2 instance

```
export APP_KEY=base64:CWSn9huySwDt+uslG/LqW4xkUqzSs6Qjd1MG+ukMO/A=
export DB_HOST=db
export DB_DATABASE=test
export DB_USERNAME=test
export DB_PASSWORD=test
export MYSQL_DATABASE=test
export MYSQL_ROOT_PASSWORD=test
export MYSQL_PASSWORD=test
export MYSQL_USER-test
```
