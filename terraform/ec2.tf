data "template_file" "startup" {
  template = file("./templates/install-required.sh")
}

#--------------------------------------------------------------
# Key Pair
#--------------------------------------------------------------
resource "tls_private_key" "tls_launch_config_key" {
  algorithm = "RSA"
}

resource "aws_key_pair" "launch_config_key_pair" {
  key_name   = "ec2-key"
  public_key = tls_private_key.tls_launch_config_key.public_key_openssh

  tags = {
    Name = "ec2-key"
  }
}

resource "aws_secretsmanager_secret" "launch_config_ec2_key_pair" {
  name                    = "/ec2/keypair/private"
  recovery_window_in_days = 0
}

resource "aws_secretsmanager_secret_version" "launch_config_ec2_key" {
  secret_id     = aws_secretsmanager_secret.launch_config_ec2_key_pair.id
  secret_string = tls_private_key.tls_launch_config_key.private_key_pem
}

#--------------------------------------------------------------
# CloudWatch
#--------------------------------------------------------------
resource "aws_cloudwatch_log_group" "instance_log_group" {
  name              = "/aws/ec2"
  retention_in_days = 30

  tags = {
    Name        = "/aws/ec2"
    Application = "CloudWatch"
  }
}

#--------------------------------------------------------------
# AMI
#--------------------------------------------------------------
data "aws_ami" "amazon_linux_2_server" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-ebs"]
  }
}

#--------------------------------------------------------------
# Instance
#--------------------------------------------------------------
resource "aws_network_interface" "nic" {
  subnet_id       = aws_subnet.public_subnet.id
  security_groups = [aws_security_group.ec2_sg.id]

  tags = {
    Name        = "nic"
    Application = "EC2"
  }
}

resource "aws_instance" "ec2" {
  disable_api_termination = false
  ami                     = data.aws_ami.amazon_linux_2_server.id
  instance_type           = "t2.micro"
  key_name                = aws_key_pair.launch_config_key_pair.key_name
  user_data               = data.template_file.startup.rendered

  network_interface {
    network_interface_id = aws_network_interface.nic.id
    device_index         = 0
  }

  ebs_block_device {
    device_name = "/dev/xvda"
    volume_size = "25"
    volume_type = "gp3"
    encrypted   = "true"
  }

  tags = {
    Name = "DeploymentTest"
  }

  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  monitoring           = true
}

#--------------------------------------------------------------
# Security Groups
#--------------------------------------------------------------
resource "aws_security_group" "ec2_sg" {
  name        = "ec2-sg"
  description = "EC2 security group"
  vpc_id      = aws_vpc.vpc.id

  tags = merge(var.default_tags, {
    Name = "ec2-sg"
  })
}

resource "aws_security_group_rule" "ec2_custom_ingress_80" {
  type              = "ingress"
  description       = "Allow HTTP"
  from_port         = 80
  to_port           = 8000
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ec2_sg.id
}

resource "aws_security_group_rule" "ec2_custom_ingress_22" {
  type              = "ingress"
  description       = "Allow HTTP"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ec2_sg.id
}

resource "aws_security_group_rule" "ec2_rule_egress_all" {
  type              = "egress"
  description       = "Allow all Egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ec2_sg.id
}
