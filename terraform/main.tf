terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.15.0"
    }
  }

  required_version = ">= 1.2.1"
}

provider "aws" {
  region = var.region
  default_tags {
    tags = merge(var.default_tags, {
      Environment = var.environment
    })
  }
}

data "aws_elb_service_account" "main" {}

data "aws_caller_identity" "current" {}

resource "random_string" "random" {
  length  = 6
  upper   = false
  special = false
}

#--------------------------------------------------------------
# VPC
#--------------------------------------------------------------
resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr_block
  instance_tenancy     = "default"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name        = "vpc"
    Description = "Terraform Managed VPC"
  }
}

#--------------------------------------------------------------
# Subnets
#--------------------------------------------------------------
resource "aws_subnet" "public_subnet" {
  # One subnet in each AZ
  vpc_id = aws_vpc.vpc.id

  # AZ = region + AZ identifier
  availability_zone = format("%s%s", var.region, "a")

  # Calculate subnet CIDR as a /24 range under the VPC CIDR
  cidr_block = cidrsubnet(var.vpc_cidr_block, 8, 1)

  map_public_ip_on_launch = true

  tags = {
    Name        = "subnet-public"
    Description = "Terraform Managed Public Subnet in ${format("%s", var.region)}"
  }
}

resource "aws_default_network_acl" "default_network_acl" {
  default_network_acl_id = aws_vpc.vpc.default_network_acl_id

  subnet_ids = [aws_subnet.public_subnet.id]

  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }
}

#--------------------------------------------------------------
# IGW
#--------------------------------------------------------------
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name        = "igw"
    Description = "Terraform Managed IGW"
  }
}

#--------------------------------------------------------------
# Route tables
#--------------------------------------------------------------
resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name        = "rtb-public"
    Description = "Terraform Managed Public RTB"
  }
}

resource "aws_route_table_association" "public_route_table_associations" {

  subnet_id      = aws_subnet.public_subnet.id
  route_table_id = aws_route_table.public_route_table.id
}

#--------------------------------------------------------------
# Security groups
#--------------------------------------------------------------
resource "aws_default_security_group" "default_security_group" {
  vpc_id = aws_vpc.vpc.id
}
