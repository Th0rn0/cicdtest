output "public_dns" {
  value       = aws_instance.ec2.public_dns
}

output "instance_id" {
  value       = aws_instance.ec2.id
}

output "private_key" {
  value     = tls_private_key.tls_launch_config_key.private_key_pem
  sensitive = true
}

