#--------------------------------------------------------------
# AWS
#--------------------------------------------------------------
variable "region" {
  type        = string
  description = "AWS region in which to deploy the resources"
  default     = "eu-west-1"
}

variable "environment" {
  type        = string
  description = "Which environment is being deployed, for example Prod or UAT"
  default     = "UAT"
}

#--------------------------------------------------------------
# Tags
#--------------------------------------------------------------
variable "default_tags" {
  type        = map(string)
  description = "(Optional) Default tags for dns pack"
  default     = {}
}

#--------------------------------------------------------------
# Networking - VPC
#--------------------------------------------------------------
variable "vpc_cidr_block" {
  type        = string
  description = "(Required) CIDR block (IP range) of the VPC"
  default     = "10.10.0.0/16"
}
