#--------------------------------------------------------------
# Instance Roles
#--------------------------------------------------------------
resource "aws_iam_instance_profile" "instance_profile" {
  name = "instance-profile"
  role = aws_iam_role.instance_role.name
}

resource "aws_iam_role" "instance_role" {
  name = "instance-role"
  path = "/"

  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : "ec2.amazonaws.com"
          },
          "Effect" : "Allow",
          "Sid" : ""
        }
      ]
    }
  )

  tags = {
    Name        = "instance-role"
    Description = "Terraform Managed Instance Role"
  }
}

resource "aws_iam_role_policy_attachment" "ec2_role_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
  role       = aws_iam_role.instance_role.name
}


resource "aws_iam_role_policy" "instance_role_policy" {
  name = "instance-role-policy"
  role = aws_iam_role.instance_role.name

  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [{
        "Sid" : "Ec2Access",
        "Effect" : "Allow",
        "Action" : [
          "ec2:AssociateAddress",
          "ec2:DescribeVolumeStatus",
          "ec2:DescribeTag",
          "ec2:DescribeVolumes",
          "ec2:DescribeVolumeAttribute",
          "ec2:DescribeInstanceAttribute",
          "ec2:DescribeInstanceStatus",
          "ec2:DescribeInstances"
        ],
        "Resource" : "*"
        },
        {
          "Sid" : "EC2TagsAccess",
          "Effect" : "Allow",
          "Action" : [
            "ec2:CreateTags"
          ],
          "Resource" : [
            "arn:aws:ec2:*:*:volume/*"
          ]
        },
        {
          "Sid" : "KMSAccess",
          "Effect" : "Allow",
          "Action" : [
            "kms:Encrypt",
            "kms:Decrypt",
            "kms:ReEncrypt*",
            "kms:GenerateDataKey*",
            "kms:DescribeKey"
          ],
          "Resource" : "*"
        },
        {
          "Sid" : "CloudWatchAccess",
          "Effect" : "Allow",
          "Action" : [
            "logs:CreateLogStream",
            "logs:PutLogEvents",
          ],
          "Resource" : "*"
        },
        {
          "Sid" : "S3Read",
          "Effect" : "Allow",
          "Action" : [
            "s3:Get*",
            "s3:List*"
          ],
          "Resource" : "*"
        },
        {
          "Sid" : "SSMParamAccess",
          "Effect" : "Allow",
          "Action" : [
            "ssm:GetParameter"
          ],
          "Resource" : [
            "arn:aws:ssm:${var.region}:${data.aws_caller_identity.current.id}:parameter/*"
          ]
        },
        {
          "Sid" : "SSMDescribeParams"
          "Effect" : "Allow"
          "Action" : [
            "ssm:DescribeParameters"
          ],
          "Resource" : "*",
        }
      ]
    }
  )
}

resource "aws_iam_role" "codedeploy_service" {
  name = "codedeploy-service-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": [
          "codedeploy.amazonaws.com"
        ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}
